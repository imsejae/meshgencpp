# ============================================================================
#  Distributed under the terms of the Berkeley Software Distribution (BSD)
#  license
#                     http://www.opensource.org/licenses/
# ============================================================================


# ============================================================================
def install_env_variables():

    import sys
    import os
    import getopt
    import glob

    qhull_dir = glob.glob("../qhull*");
    if len(qhull_dir)==0:
        qhull_dir = ["EMPTY"];

    # Paths
    if os.path.exists(qhull_dir[0]):
        qhull_path = os.path.abspath(qhull_dir[0]);
    else:
        print ("")
        qhull_tmp = input(' Enter path to qhull package: ')
        if os.path.exists(qhull_tmp):
            qhull_path = os.path.abspath(qhull_tmp);
            print ("");
            print (" Found qhull directory: ",qhull_path);
            print ("");
        else:
            print ("");
            print ("Could not find qhull directory: ",qhull_tmp);
            print ("");
            print ("Make sure that qhull has been properly installed");
            print ("  and that you provided the proper path.");
            print ("");
            print ("Once you are certain qhull is installed and you know");
            print ("  the proper path, please re-run installation by typing:");
            print ("");
            print ("$ make install");
            print ("");
            exit();

    meshgen_path = os.path.abspath(os.curdir);
    outfile_base="setenv";

    # Open output files
    csh_file_name  = outfile_base + "." + "csh";
    csh_file  = open(csh_file_name,'w+');
    bash_file_name = outfile_base + "." + "bash";
    bash_file = open(bash_file_name,'w+');

    # Write out boiler plate
    boiler_plate = ("# MeshGenC++ environment settings\n");
    csh_file.write(boiler_plate);
    bash_file.write(boiler_plate);

    # Write out variables
    print ("");
    print ("The following variables will be defined:");
    print ("  MESHGENCPP = %s" % meshgen_path);
    print ("  QHULL = %s" % qhull_path);
    write_environment_variable(csh_file,bash_file,"MESHGENCPP",meshgen_path);
    write_environment_variable(csh_file,bash_file,"QHULL",qhull_path);
    pmesh2_command = "python $MESHGENCPP/viz/python/plotmesh2.py";
    print ("  plotmesh2 = %s" %pmesh2_command);
    write_shortcut_alias(csh_file,bash_file,"plotmesh2",pmesh2_command);

    # Append .cshrc and .bashrc files
    home = os.environ['HOME'];
    home_cshrc  = home + "/.cshrc";
    home_bashrc = home + "/.bashrc";

    f1 = open(home_cshrc,'a');
    csh_file.seek(0);
    f1.write('\n');
    f1.write('# >>>>>>>>>>>>>> added by MeshGenC++ installer <<<<<<<<<<<<<<\n');
    f1.write(csh_file.read());
    f1.write('# >>>>>>>>>>>>>> added by MeshGenC++ installer <<<<<<<<<<<<<<\n');
    f1.close();

    f2 = open(home_bashrc,'a');
    bash_file.seek(0);
    f2.write('\n');
    f2.write('# >>>>>>>>>>>>>> added by MeshGenC++ installer <<<<<<<<<<<<<<\n');
    f2.write(bash_file.read());
    f2.write('# >>>>>>>>>>>>>> added by MeshGenC++ installer <<<<<<<<<<<<<<\n');
    f2.close();

    # Close output files
    csh_file.close();
    bash_file.close();

    # Remove output files
    os.remove(csh_file_name);
    os.remove(bash_file_name);

    # Write message to user
    print ("");
    print ("Environment variables were successfully defined.");
    print ("In order to activate them, type one of the following:");
    print ("");
    print ("# If you are running a C-shell:")
    print ("$ source " + home_cshrc);
    print ("");
    print ("# If you are running a Bourne shell:")
    print ("$ source " + home_bashrc);
    print ("");
# ============================================================================

# ============================================================================
def write_environment_variable(csh_handle,bash_handle,var,value):
    csh_handle.write( 'setenv %s "%s"\n' % (var.upper(),value))
    bash_handle.write('export %s="%s"\n' % (var.upper(),value))
# ============================================================================

# ============================================================================
def write_shortcut_alias(csh_handle,bash_handle,var,value):
    csh_handle.write( 'alias %s "%s"\n' % (var,value))
    bash_handle.write('alias %s="%s"\n' % (var,value))
# ============================================================================

#----------------------------------------------------------
if __name__== '__main__':
    """Installation script for MeshGenC++ code:

        python util/install.py

    """

    # call the main routine
    install_env_variables();
#----------------------------------------------------------
