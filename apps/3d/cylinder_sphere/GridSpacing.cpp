#include "meshdefs.h"
#include "dog_math.h"

//  Grid spacing function: 
//
//     This functions sets a relative grid spacing of points
//
//     The input is a point (i.e., x and y coordinate), and the
//     output is a real positive number called "hdist". Large (small)
//     "hdist" means relatively large (small) grid spacing (relative to
//     the maximum and minimum values of GridSpacing.cpp).
//
//     For example, GridSpacing = 1   for all input x and y and
//                  GridSpacing = 55  for all input x and y 
//
//           will produce the same nearly uniform mesh since 
//           the relative variation of GridSpacing in both
//           examples is zero.
//
double GridSpacing(point pt)
{
  double x = pt.x;
  double y = pt.y;
  double z = pt.z;
  double r = sqrt(x*x + y*y);
  double rho = sqrt(x*x + y*y + z*z);
  
  double d1,d2,d3,d4;
  
  d1 = 1.0e0-z;
  d2 = z+1.0e0;
  d3 = 1.0e0-r;
  d4 = rho-0.5e0;
  
  double hdist;
  hdist = 2.0e0 + Min( Min(d1,d2), Min(d3,d4) );
  
  return hdist;
}
