#include "meshdefs.h"
#include "dog_math.h"

//  Signed distance function: 
//
//      SignedDistance(x,y) < 0 inside the region
//      SignedDistance(x,y) = 0 on the boundary
//      SignedDistance(x,y) > 0 outside of the region
//
double SignedDistance(point pt)
{
  double x = pt.x;
  double y = pt.y;
  double z = pt.z;
  double r = sqrt(x*x + y*y);
  double rho = sqrt(x*x + y*y + z*z);
  
  double d1,d2,d3,d4,dist;
  
  d1 = 1.0e0-z;
  d2 = z+1.0e0;
  d3 = 1.0e0-r;
  d4 = rho-0.5e0;
  
  dist = -Min( Min(d1,d2), Min(d3,d4) );
  
  return dist;
}
