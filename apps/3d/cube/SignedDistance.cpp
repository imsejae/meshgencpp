#include "meshdefs.h"
#include "dog_math.h"

//  Signed distance function: 
//
//      SignedDistance(x,y) < 0 inside the region
//      SignedDistance(x,y) = 0 on the boundary
//      SignedDistance(x,y) > 0 outside of the region
//
double SignedDistance(point pt)
{
  double x = pt.x;
  double y = pt.y;
  double z = pt.z;
  
  double dx = Min( 1.0e0-x, x );
  double dy = Min( 1.0e0-y, y );
  double dz = Min( 1.0e0-z, z );

  double dist = -Min( Min(dx,dy), dz );
  
  return dist;
}
