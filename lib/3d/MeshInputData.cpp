#include "meshdefs.h"
#include <cstdio>
#include <assert.h>

// Read-in input data
void MeshInputData(double& h0, 
		   point& xyzmin, 
		   point& xyzmax, 
		   int& numfixpts, 
		   point*& fixpt,
		   int& maxiters,
		   char*& GridType)
{
  FILE* infile = fopen("input3D.data","r");
  char buffer[256];

  int garbage;

  garbage=fscanf(infile,"%s",GridType);  
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  garbage=fscanf(infile,"%lf",&h0); 
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  garbage=fscanf(infile,"%i",&maxiters);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  garbage=fscanf(infile,"%lf",&xyzmin.x);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  garbage=fscanf(infile,"%lf",&xyzmax.x);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  garbage=fscanf(infile,"%lf",&xyzmin.y);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  garbage=fscanf(infile,"%lf",&xyzmax.y);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  garbage=fscanf(infile,"%lf",&xyzmin.z);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  garbage=fscanf(infile,"%lf",&xyzmax.z);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  garbage=fscanf(infile,"%i",&numfixpts);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  fixpt = new point[numfixpts];
  for (int i = 0; i<numfixpts; i++)
    {
      garbage=fscanf(infile,"%lf %lf %lf",&fixpt[i].x,&fixpt[i].y,&fixpt[i].z);
    }
  fclose(infile);

  return;
}
