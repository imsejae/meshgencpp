#include "meshdefs.h"
#include <cstdio>
#include <assert.h>

// Read-in input data
void MeshInputData(double& h0, 
		   point& xymin, 
		   point& xymax, 
		   int& numfixpts, 
		   point*& fixpt, 
		   int& maxiters, 
		   int& sub_factor, 
		   char*& GridType)
{
  FILE* infile = fopen("input2D.data","r");
  char buffer[256];

  int garbage;

  garbage=fscanf(infile,"%s",GridType);  
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  garbage=fscanf(infile,"%lf",&h0);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  garbage=fscanf(infile,"%i",&maxiters);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  garbage=fscanf(infile,"%i",&sub_factor);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  garbage=fscanf(infile,"%lf",&xymin.x);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  garbage=fscanf(infile,"%lf",&xymax.x);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  garbage=fscanf(infile,"%lf",&xymin.y);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  garbage=fscanf(infile,"%lf",&xymax.y);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  garbage=fscanf(infile,"%i",&numfixpts);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);
  assert(fgets(buffer, sizeof buffer, infile)!=NULL);

  fixpt = new point[numfixpts];
  for (int i = 0; i<numfixpts; i++)
    {  
      garbage=fscanf(infile,"%lf %lf",&fixpt[i].x,&fixpt[i].y);
    }
  
  fclose(infile);
}
