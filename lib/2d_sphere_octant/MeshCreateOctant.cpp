#include "meshdefs.h"
#include <stdio.h>

void MeshCreateOctant(const double radius,
                      int& numpts,
                      int& numtri,
                      point*& p,
                      triangle*& t)
{
  p = new point[6];
  t = new triangle[8];
  numpts = 6;
  numtri = 8;

  // Points and elements for a regular icosahedron
  p[0].x =  1.0;   p[0].y =  0.0;   p[0].z =  0.0;
  p[1].x =  0.0;   p[1].y =  1.0;   p[1].z =  0.0;
  p[2].x = -1.0;   p[2].y =  0.0;   p[2].z =  0.0;
  p[3].x =  0.0;   p[3].y = -1.0;   p[3].z =  0.0;
  p[4].x =  0.0;   p[4].y =  0.0;   p[4].z =  1.0;
  p[5].x =  0.0;   p[5].y =  0.0;   p[5].z = -1.0;

  t[0].n1 = 0;   t[0].n2 = 1;   t[0].n3 = 4;
  t[1].n1 = 1;   t[1].n2 = 2;   t[1].n3 = 4;
  t[2].n1 = 2;   t[2].n2 = 3;   t[2].n3 = 4;
  t[3].n1 = 0;   t[3].n2 = 3;   t[3].n3 = 4;
  t[4].n1 = 0;   t[4].n2 = 1;   t[4].n3 = 5;
  t[5].n1 = 1;   t[5].n2 = 2;   t[5].n3 = 5;
  t[6].n1 = 2;   t[6].n2 = 3;   t[6].n3 = 5;
  t[7].n1 = 0;   t[7].n2 = 3;   t[7].n3 = 5;

  // Scale points so that we have a sphere of radius "radius"
  for (int i=0; i<6; i++)
    {
      p[i].x = radius * p[i].x;
      p[i].y = radius * p[i].y;
      p[i].z = radius * p[i].z;
    }

}
